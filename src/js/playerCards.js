export const calculatePlayerCards = (numOfPlayers) => {
  let cultists;

  if (numOfPlayers < 4) {
    console.error('Sorry you need more players to play the minimum is 4');
    return;
  } else if (numOfPlayers >= 4 && numOfPlayers < 7) {
    cultists = 2;
  } else if (numOfPlayers >= 7 && numOfPlayers < 9) {
    cultists = 3;
  } else if (numOfPlayers > 8) {
    console.error('Sorry you have too many players the maximum is 8');
    return;
  }

  const investigators = (numOfPlayers === 6) ? 4 : (numOfPlayers - cultists) + 1;
  const playerCards = {
    investigators,
    cultists
  }

  return playerCards;
};

export const buildPlayerCardsDeck = (playerCards) => {
  const { investigators, cultists } = playerCards;
  const playerCardDeck = [];

  for (let i = 0; i < investigators; i++) {
    playerCardDeck.push('investigator');
  }

  for (let i = 0; i < cultists; i++) {
    playerCardDeck.push('cultist');
  }

  return playerCardDeck;
};