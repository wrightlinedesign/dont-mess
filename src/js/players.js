export const players = [];

export const addPlayer = (name) => {
  const playerObject = {
    name,
    allegiance: '',
    hand: []
  };

  players.push(playerObject);
};