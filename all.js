const calculateDeck = (numOfPlayers) => {
  const branches = numOfPlayers;
  const squid = 1;
  const rocks = Math.abs((numOfPlayers * 5) - (branches + squid));

  const deckNumbers = {
    branches,
    squid,
    rocks
  };

  return deckNumbers;
};

const buildDeck = (deckNumbers) => {
  const deck = [];
  const { branches, squid, rocks } = deckNumbers;

  for (let i = 0; i < branches; i++) {
    deck.push('branch');
  }

  for (let i = 0; i < rocks; i++) {
    deck.push('rock');
  }

  for (let i = 0; i < squid; i++) {
    deck.push('squid');
  }

  return deck;
};

const calculatePlayerCards = (numOfPlayers) => {
  let cultists;

  if (numOfPlayers < 4) {
    console.error('Sorry you need more players to play the minimum is 4');
    return;
  } else if (numOfPlayers >= 4 && numOfPlayers < 7) {
    cultists = 2;
  } else if (numOfPlayers >= 7 && numOfPlayers < 9) {
    cultists = 3;
  } else if (numOfPlayers > 8) {
    console.error('Sorry you have too many players the maximum is 8');
    return;
  }

  const investigators = (numOfPlayers === 6) ? 4 : (numOfPlayers - cultists) + 1;
  const playerCards = {
    investigators,
    cultists
  }

  return playerCards;
};

const buildPlayerCardsDeck = (playerCards) => {
  const { investigators, cultists } = playerCards;
  const playerCardDeck = [];

  for (let i = 0; i < investigators; i++) {
    playerCardDeck.push('investigator');
  }

  for (let i = 0; i < cultists; i++) {
    playerCardDeck.push('cultist');
  }

  return playerCardDeck;
};

const shuffle = (deck) => {
  let count = deck.length;
  while(count) {
    deck.push(deck.splice(Math.floor(Math.random() * count), 1)[0]);
    count -= 1;
  }

  // console.log(deck);
  return deck;
};

const deal = (deck, players, numCards) => {
  players.forEach(player => {
    if (deck[0] === 'investigator' || deck[0] === 'cultists') {
      while(player.allegiance === '') {
        player.allegiance = (deck.splice(Math.floor(Math.random() * deck.length), 1)[0]);
      }
    } else {
      while(player.hand.length !== numCards) {
        player.hand.push(deck.splice(Math.floor(Math.random() * deck.length), 1)[0]);
      }
    }
  });
}

const players = [
  {
    name: 'player 1',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 2',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 3',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 4',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 5',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 6',
    allegiance: '',
    hand: []
  },
  {
    name: 'player 7',
    allegiance: '',
    hand: []
  },
];

const playerHands = shuffle(buildDeck(calculateDeck(players.length)));
const playerCardDeck = shuffle(buildPlayerCardsDeck(calculatePlayerCards(players.length)));

deal(playerCardDeck, players, 1);
deal(playerHands, players, 5);

console.log(players);