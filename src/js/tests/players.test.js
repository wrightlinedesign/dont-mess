import { players, addPlayer } from '../players';

describe('players.js', () => {
  it('players should be an array', () => {
    expect(players).toEqual(expect.any(Array));
  });

  describe('addPlayer', () => {
    it('should add a player to the players array', () => {
      addPlayer('Player 1');
      expect(players.length).toBeGreaterThan(0);
    });

    it('player should have name, empty hand and no allegiance', () => {
      expect(players[0]).toEqual({
        name: 'Player 1',
        allegiance: '',
        hand: []
      });
    });
  });
});
