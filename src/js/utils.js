export const shuffle = (deck) => {
  let count = deck.length;
  while(count) {
    deck.push(deck.splice(Math.floor(Math.random() * count), 1)[0]);
    count -= 1;
  }

  // console.log(deck);
  return deck;
};

export const deal = (deck, players, numCards) => {
  players.forEach(player => {
    if (deck[0] === 'investigator' || deck[0] === 'cultists') {
      while(player.allegiance === '') {
        player.allegiance = (deck.splice(Math.floor(Math.random() * deck.length), 1)[0]);
      }
    } else {
      while(player.hand.length !== numCards) {
        player.hand.push(deck.splice(Math.floor(Math.random() * deck.length), 1)[0]);
      }
    }
  });
};