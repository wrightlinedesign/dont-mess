export const calculateDeck = (numOfPlayers) => {
  const branches = numOfPlayers;
  const squid = 1;
  const rocks = Math.abs((numOfPlayers * 5) - (branches + squid));

  const deckNumbers = {
    branches,
    squid,
    rocks
  };

  return deckNumbers;
};

export const buildDeck = (deckNumbers) => {
  const deck = [];
  const { branches, squid, rocks } = deckNumbers;

  for (let i = 0; i < branches; i++) {
    deck.push('branch');
  }

  for (let i = 0; i < rocks; i++) {
    deck.push('rock');
  }

  for (let i = 0; i < squid; i++) {
    deck.push('squid');
  }

  return deck;
};